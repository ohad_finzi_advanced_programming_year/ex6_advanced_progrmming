#include <iostream>


#define FORBIDDEN_NUMBER 8200


int add(int a, int b, bool& check)
{
    if (a + b == FORBIDDEN_NUMBER)  //checks if the return value of the function is equal 8200
    {
        check = true;
    }

    return a + b;
}


int multiply(int a, int b, bool& check)
{
    int sum = 0;

    for (int i = 0; i < b; i++)
    {
        sum = add(sum, a, check);
    };

    return sum;
}


int pow(int a, int b, bool& check)
{
    int exponent = 1;

    for (int i = 0; i < b; i++)
    {
        exponent = multiply(exponent, a, check);
    };

    return exponent;
}


int main(void)
{
    bool check = false;  //will be true at the end of the function if the number 8200 will be used 
    int ans = multiply(8200, 1, check);

    if (check)
    {
        std::cout << "This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year." << std::endl;
    }
    else
    {
        std::cout << ans << std::endl;
    }

    return 0;
}

