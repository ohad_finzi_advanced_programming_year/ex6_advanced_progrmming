#include <iostream>


#define FORBIDDEN_NUMBER 8200


int add(int a, int b)
{
    if (a + b == FORBIDDEN_NUMBER)  //throws an exception if the return value of the function is 8200
    {
        throw std::string("This user is not authorized to access 8200, please enter different numbers, or try to get clearance in 1 year.");
    }

    return a + b;
}


int multiply(int a, int b)
{
    int sum = 0;

    for (int i = 0; i < b; i++)
    {
        sum = add(sum, a);
    };

    return sum;
}


int pow(int a, int b)
{
    int exponent = 1;

    for (int i = 0; i < b; i++)
    {
        exponent = multiply(exponent, a);
    };

    return exponent;
}


int main(void)
{
    try
    {
        std::cout << multiply(8200, 1) << std::endl;
    }  //catch incase there is an exception
    catch (std::string& errorMsg)
    {
        std::cout << errorMsg << std::endl;
    }

    return 0;
}

