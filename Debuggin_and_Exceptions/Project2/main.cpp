#include <iostream>
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "rectangle.h"
#include "parallelogram.h"
#include <string>
#include "shapeException.h"
#include "inputException.h"
#include "Pentagon.h"
#include "Hexagon.h"

char inputChar();

int main()
{
	std::string nam, col; double rad = 0, ang = 0, ang2 = 180; int height = 0, width = 0, side = 0;
	Circle circ(col, nam, rad);
	quadrilateral quad(nam, col, width, height);
	rectangle rec(nam, col, width, height);
	parallelogram para(nam, col, width, height, ang, ang2);
	Hexagon hex(nam, col, side);
	Hexagon pen(nam, col, side);

	Shape *ptrcirc = &circ;
	Shape *ptrquad = &quad;
	Shape *ptrrec = &rec;
	Shape *ptrpara = &para;
	Shape* ptrhex = &hex;
	Shape* ptrpen = &pen;
	
	std::cout << "Enter information for your objects" << std::endl;
	char shapetype;
	char x = 'y';
	while (x != 'x') {
		std::cout << "which shape would you like to work with?.. \nc=circle, q = quadrilateral, r = rectangle, p = parallelogram, h = hexagon, i = pentagon" << std::endl;
		shapetype = inputChar();
try
		{


			switch (shapetype) {
			case 'c':
				std::cout << "enter color, name,  rad for circle" << std::endl;
				std::cin >> col >> nam >> rad;
				circ.setColor(col);
				circ.setName(nam);
				circ.setRad(rad);
				ptrcirc->draw();
				break;
			case 'q':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				quad.setName(nam);
				quad.setColor(col);
				quad.setHeight(height);
				quad.setWidth(width);
				ptrquad->draw();
				break;
			case 'r':
				std::cout << "enter name, color, height, width" << std::endl;
				std::cin >> nam >> col >> height >> width;
				rec.setName(nam);
				rec.setColor(col);
				rec.setHeight(height);
				rec.setWidth(width);
				ptrrec->draw();
				break;
			case 'p':
				std::cout << "enter name, color, height, width, 2 angles" << std::endl;
				std::cin >> nam >> col >> height >> width >> ang >> ang2;
				para.setName(nam);
				para.setColor(col);
				para.setHeight(height);
				para.setWidth(width);
				para.setAngle(ang, ang2);
				ptrpara->draw();
				break;
			case 'i':
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> side;
				pen.setName(nam);
				pen.setColor(col);
				pen.setSide(side);
				ptrpen->draw();
				break;

			case 'h':
				std::cout << "enter name, color, side" << std::endl;
				std::cin >> nam >> col >> side;
				hex.setName(nam);
				hex.setColor(col);
				hex.setSide(side);
				ptrhex->draw();
				break;

			default:
				std::cout << "you have entered an invalid letter, please re-enter" << std::endl;
				break;
			}
			std::cout << "would you like to add more object press any key if not press x" << std::endl;
			std::cin.get() >> x;
		}
		catch (const inputException& e)
		{
			std::cin.clear();  //clears the screen
			std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');  //ignores the input

			std::cout << e.what() << std::endl;
		}
		catch (const shapeException& e)
		{
			std::cout << e.what() << std::endl;
		}
		catch (...)
		{
			printf("caught a bad exception. continuing as usual\n");
		}
	}



		system("pause");
		return 0;
	
}


/*
The function gets a string input from the user and returns only its first character
Output: the first char of a string
*/
char inputChar()
{
	std::string stringInput = "";

	std::cin.clear();  //cleans the buffer before using string input
	std::getline(std::cin, stringInput);

	if (stringInput.length() != 1)  //checks if the user enterd more than one digit
	{
		std::cout << "Warning - Don't try to build more than one shape at once" <<std::endl;
	}

	return stringInput[0];   //returns the first char of the string
}

