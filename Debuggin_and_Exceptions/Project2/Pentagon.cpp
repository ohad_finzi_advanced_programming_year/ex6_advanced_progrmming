#include "Pentagon.h"


/*
The function constructs the current pentagon class according to the given elements
Input: name of the shape, color and the length of side
*/
Pentagon::Pentagon(std::string name, std::string color, double side) :
	Shape(name, color)
{
	setSide(side);
}


/*
The function prints the details about the current pentagon class
*/
void Pentagon::draw()
{
	std::cout << std::endl << "Color is " << getColor() << std::endl << "Name is " << getName() << std::endl << "Side is " << getSide() << std::endl << "Area is " << CalArea() << std::endl;
}


/*
The function used the function calcPentagonArea from the MathUtils class to get the area of the current pentagon
Output: the current pentagon area
*/
double Pentagon::CalArea()
{
	return MathUtils::CallPentagonArea(_side);
}


/*
The function sets side of the current pentagon class
Input: the new side of the current pentagon class
*/
void Pentagon::setSide(double side)
{
	if (side < 0)  //checks if the radius is a positive number
	{
		throw shapeException();
	}
	if (std::cin.fail())  //cause an exception whenever the cin failed
	{
		throw inputException();
	}

	_side = side;
}


/*
The function returns the side of the current pentagon class
*/
double Pentagon::getSide()
{
	return _side;
}

