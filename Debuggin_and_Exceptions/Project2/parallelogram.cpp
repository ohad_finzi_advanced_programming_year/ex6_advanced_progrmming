
#include "parallelogram.h"
#include "shape.h"
#include "circle.h"
#include "quadrilateral.h"
#include "shapeException.h"
#include "inputException.h"
#include <iostream>


parallelogram::parallelogram(std::string col, std::string nam, int h, int w, double ang, double ang2):quadrilateral(col, nam, h, w) {
	 setAngle(ang, ang2);
}
void parallelogram::draw()
{
	std::cout <<getName()<< std::endl << getColor() << std::endl << "Height is " << getHeight() << std::endl<< "Width is " << getWidth() << std::endl
		<< "Angles are: " << getAngle()<<","<<getAngle2()<< std::endl <<"Area is "<<CalArea(getWidth(),getHeight())<< std::endl;
}

double parallelogram::CalArea(double w, double h) {
	return w*h;
}
void parallelogram::setAngle(double ang, double ang2) {
	if ((ang + ang2 != 180) || (ang < 0) || (ang2 < 0) || (ang > 180) || (ang2 > 180))  //throws an excpetion if the sum of the angles are different than 180, or one of them is not betweeen 0 and 180
	{
		throw shapeException();
	}
	if (std::cin.fail())  //cause an exception whenever the cin failed
	{
		throw inputException();
	}

	angle = ang;
	angle2 = ang2;
}
double parallelogram::getAngle() {
	return angle;
}
double parallelogram::getAngle2() {
		return angle2;
	}
