#pragma once

#include <cmath>


class MathUtils
{
public:
	static double CallPentagonArea(double side);
	static double CallHexagonArea(double side);

};

