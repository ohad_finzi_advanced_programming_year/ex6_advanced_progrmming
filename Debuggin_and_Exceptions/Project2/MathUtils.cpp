#include "MathUtils.h"


/*
Functions which caluclates the area of a pentagon according to its formula
Input: the side of the pentagon
Output: the area of the pentagon according to its side
*/
double MathUtils::CallPentagonArea(double side)
{
	return (sqrt(5 * (5 + 2 * (sqrt(5)))) * side * side) / 4;
}


/*
Functions which caluclates the area of a hexagon according to its formula
Input: the side of the hexagon
Output: the area of the hexagon according to its side
*/
double MathUtils::CallHexagonArea(double side)
{
	return ((3 * sqrt(3) * (side * side)) / 2);
}

